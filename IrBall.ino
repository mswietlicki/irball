// This #include statement was automatically added by the Particle IDE.
#include "IRRemoteMicrolab.h"
#include "IRRemoteBenq.h"

IRRemoteMicrolab speakersRemote(D1);
IRRemoteBenq projectorRemote(D1);

int callSpeakers(String command);
int callProjector(String command);

void setup() {
	Spark.function("Speakers", callSpeakers);
	Spark.function("Video", callProjector);
}

void loop() {
}

int callSpeakers(String command){
	speakersRemote.SendCommand(command);
	return 0;
}

int callProjector(String command){
	projectorRemote.SendCommand(command);
	return 0;
}
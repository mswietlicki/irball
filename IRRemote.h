/*
 * IRremote
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * For details, see http://arcfn.com/2009/08/multi-protocol-infrared-remote-library.htm http://arcfn.com
 * Edited by Mitra to add new controller SANYO
 *
 * Interrupt code based on NECIRrcv by Joe Knapp
 * http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1210243556
 * Also influenced by http://zovirl.com/2008/11/12/building-a-universal-remote-with-an-arduino/
 *
 * JVC and Panasonic protocol added by Kristian Lauszus (Thanks to zenwheel and other people at the original blog post)
 */

#ifndef IRRemote_h
#define IRRemote_h
#include "application.h"

class IRRemote
{
  const int irPin;
  int burstWait;
  int burstLength;

public:
  IRRemote(int irPin);
  void setup(int freq, int nbits, int hdr_mark, int hdr_space, int bit_mark, int one_space, int zero_space);
  void send(unsigned long data);
private:
  void enableIROut(int khz);
  void mark(int usec);
  void space(int usec);
  int _freq, _nbits, _hdr_mark, _hdr_space, _bit_mark, _one_space, _zero_space;

};

#define TOPBIT 0x80000000

#endif
